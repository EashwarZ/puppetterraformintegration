package { 'mysql-server':
  ensure => installed,
}

service { 'mysql':
  ensure    => running,
  enable    => true,
  subscribe => Package['mysql-server'],
}