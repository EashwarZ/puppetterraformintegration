provider "aws" {
  region = "us-east-1"
}

resource "aws_instance" "puppet" {
  ami           = "ami-0440d3b780d96b29d"
  instance_type = "t2.micro"

  provisioner "remote-exec" {
    inline = [
      "sudo apt-get update",
      "sudo apt-get install -y puppet",
      "sudo sh -c 'echo \"172.30.28.195 puppet\" >> /etc/hosts'",
      "sudo systemctl enable puppet",
      "sudo systemctl start puppet",
      "sudo /opt/puppetlabs/bin/puppet agent --test"

    ]
  }
}
