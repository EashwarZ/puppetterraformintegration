
- **Overview**

This project demonstrates the integration between Terraform and Puppet for automating infrastructure provisioning and configuration management.


**- Components**
        Terraform: Used for infrastructure provisioning.
        Puppet: Used for configuration management.


- **Folder Structure**

        terraform/: Contains Terraform configuration files.
        puppet/: Contains Puppet manifests.

**- Prerequisites**
        Terraform: Install Terraform on your system. Download Terraform
        Puppet: Ensure Puppet is installed and configured properly. Puppet Installation Guide

**- Usage**
- [ ] 1. Clone this repository to your local machine.
- [ ] 1. Navigate to the terraform/ directory and run terraform init to initialize the Terraform configuration.
- [ ] 1. Run terraform apply to provision the infrastructure.
- [ ] 1. Once the infrastructure is provisioned, navigate to the puppet/ directory.
- [ ] 1. Update the Puppet manifests (*.pp files) according to your configuration requirements.
- [ ] 1. Apply the Puppet manifests using the Puppet agent to configure the provisioned infrastructure.


- **Integration**

        Terraform and Puppet Integration: Use Terraform provisioners or external scripts to trigger Puppet runs after Terraform provisioning.

**Testing**
        Verify that the provisioned infrastructure meets the desired configuration state defined in the Puppet manifests.       
        Test the integration by provisioning infrastructure using Terraform and ensuring that Puppet successfully configures the provisioned resources.
